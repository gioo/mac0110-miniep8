function troca(v,i,j)
    aux=v[i]
    v[i]=v[j]
    v[j]=aux
end

function insertion(v)
    for i in 1:length(v)
        j=i
        while j>1
            if compareByValueAndSuit(v[j],v[j-1])
                troca(v,j,j-1)
            else
                break
            end
            j-=1
        end
    end
    return v
end

function compareByValue(x,y)
    a=x
    b=y
    carta=x
    compara=0
    i=1
    while i<3
        if string(carta[1])=="J"
            compara=11
        elseif string(carta[1])=="Q"
            compara=12
        elseif string(carta[1])=="K"
            compara=13
        elseif string(carta[1])=="A"
            compara=14
        elseif length(carta)==2
            compara=parse(Int,string(carta[1]))
        else
            compara=10
        end
        if i==1
            a=compara
            carta=y
        else
            b=compara
        end
        i+=1
    end
    if b>a
        return true
    else
        return false
    end
end

function teste_parte1()
    if !compareByValue("2♠","A♠")||
        compareByValue("K♥", "10♥")||
        compareByValue("10♠", "10♥")
        println("Algo de errado com a função compareByValue")
    else
        println("Tudo Ok :)")
    end
end

function compareByValueAndSuit(x,y)
    a=x
    b=y
    carta=x
    compara=0
    i=1
    while i<3
        if string(carta[length(carta)])=="♣"
            compara=4
        elseif string(carta[length(carta)])=="♥"
            compara=3
        elseif string(carta[length(carta)])=="♠"
            compara=2
        elseif string(carta[length(carta)])=="♦"
            compara=1
        end
        if i==1
            a=compara
            carta=y
        else
            b=compara
        end
        i+=1
    end
    if b>a || (b==a && compareByValue(x,y))
        return true
    else
        return false
    end
end

function teste_parte2()
    if insertion(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"])!=["10♦", "J♠", "K♠", "A♠", "A♠","10♥"] ||
       insertion(["10♣","9♠","8♦","7♥","6♦","5♥","4♥","3♦","2♠","1♦"])!=["1♦","3♦","6♦","8♦","2♠","9♠","4♥","5♥","7♥","10♣"] ||
       insertion(["J♣","3♦","8♦","K♦","9♠","7♥","7♣","8♠","6♦","4♥","10♥","5♥","A♠","1♣","Q♠","1♦"])!=["1♦","3♦","6♦","8♦","K♦","8♠","9♠","Q♠","A♠","4♥","5♥","7♥","10♥","1♣","7♣","J♣"]
        println("Algo de errado com a função compareByValueAndSuit")
    else
        println("Tudo Ok :)")
    end
end
